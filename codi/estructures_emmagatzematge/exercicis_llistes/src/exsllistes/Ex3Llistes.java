package exsllistes;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

public class Ex3Llistes {
	public static void main(String[] args) {
		ex10Vector();
		ex10Llista();
	}
	
	public static void ex10Llista() {
		String llenguatges[] = {"Java", "C", "C++", "Visual Basic", "PHP", "Python",
				"Perl", "C#", "JavaScript","Delphi"};
		List<String> llista = Arrays.asList(llenguatges);
		// També:
		// llista = Arrays.asList("Java", "C", "C++"...);
		ListIterator<String> it;
		int i;
		
		System.out.println("a) El llenguatge més popular és el "+llista.get(0));
		System.out.println("b) El sisè més popular és el "+llista.get(5));
		System.out.print("c) Els tres més populars són ");
		for (i=0; i<3; i++)
		    System.out.print(llista.get(i)+" ");
		System.out.println();
		
		System.out.print("c) Els tres més populars són ");
		it = llista.listIterator();
		for (i=0; i<3; i++) {
			if (it.hasNext()) {
				System.out.print(it.next()+" ");
			}
		}
		System.out.println();

		System.out.print("d) Els tres menys populars són ");
		for (i=llista.size()-1; i >= llista.size()-3; i--)
		    System.out.print(llista.get(i)+" ");
		System.out.println();
		
		System.out.print("d) Els tres menys populars són ");
		it = llista.listIterator(llista.size());
		for (i=0; i<3; i++) {
			if (it.hasPrevious()) {
				System.out.println(it.previous()+" ");
			}
		}
		System.out.println();

		System.out.print("e) Tots menys el primer: ");
		for (i=1; i<llista.size(); i++)
		    System.out.print(llista.get(i)+" ");
		System.out.println();
		
		System.out.print("e) Tots menys el primer: ");
		it = llista.listIterator(1);
		while (it.hasNext())
		    System.out.print(it.next()+" ");
		System.out.println();

		System.out.println("f)");
		i=0;
		for (String llenguatge : llista) {
		    System.out.println((i+1)+": "+llenguatge);
		    i++;
		}
		System.out.println();

		System.out.println("g) Nombre total de llenguatges: "+llista.size());
	}
	
	public static void ex10Vector() {
		int i;
		String llenguatges[] = {"Java", "C", "C++", "Visual Basic", "PHP", "Python",
				"Perl", "C#", "JavaScript","Delphi"};

		System.out.println("a) El llenguatge més popular és el "+llenguatges[0]);
		System.out.println("b) El sisè més popular és el "+llenguatges[5]);
		System.out.print("c) Els tres més populars són ");
		for (i=0; i<3; i++)
		    System.out.print(llenguatges[i]+" ");
		System.out.println();

		System.out.print("d) Els tres menys populars són ");
		for (i=llenguatges.length-1; i >= llenguatges.length-3; i--)
		    System.out.print(llenguatges[i]+" ");
		System.out.println();

		System.out.print("e) Tots menys el primer: ");
		for (i=1; i<llenguatges.length; i++)
		    System.out.print(llenguatges[i]+" ");
		System.out.println();

		System.out.println("f)");
		for (i=0; i<llenguatges.length; i++)
		    System.out.println((i+1)+": "+llenguatges[i]);
		System.out.println();
		
		System.out.println("f)");
		i=0;
		for (String llenguatge : llenguatges) {
		    System.out.println((i+1)+": "+llenguatge);
		    i++;
		}
		System.out.println();

		System.out.println("g) Nombre total de llenguatges: "+llenguatges.length);
	}

}
