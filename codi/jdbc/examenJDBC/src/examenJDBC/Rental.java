package examenJDBC;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Rental {
	public final int id;
	public final int inventoryId;
	public final int customerId;
	public final int staffId;
	
	public Rental(int id, int inventoryId, int customerId, int staffId) {
		this.id = id;
		this.inventoryId = inventoryId;
		this.customerId = customerId;
		this.staffId = staffId;
	}
	
	public static Rental byInventory(Inventory inventory) throws SQLException {
		Rental rental = null;
		try (
			PreparedStatement st = DB.connection().prepareStatement("select rental_id, inventory_id, customer_id, staff_id from rental where inventory_id = ? and return_date is null order by rental_date desc limit 1");
		) {
			st.setInt(1, inventory.id);
			ResultSet rs = st.executeQuery();
			if (rs.next()) {
				rental = new Rental(rs.getInt(1), rs.getInt(2), rs.getInt(3), rs.getInt(4));
			}
			rs.close();
		}
		return rental;
	}
	
	public void close() throws SQLException {
		try (
			PreparedStatement st = DB.connection().prepareStatement("update rental set return_date = now() where rental_id = ?");
		) {
			st.setInt(1, id);
			st.executeUpdate();
		}
	}
	
	public Customer customer() throws SQLException {
		return Customer.byId(customerId);
	}
}
