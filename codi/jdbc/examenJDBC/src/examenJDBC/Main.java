package examenJDBC;

import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class Main {
	private Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		Main main = new Main();
		main.run();
	}

	public void run() {
		int option = 0;
		Staff staff;
		try {
			staff=login();
			while (option!=3) {
				option = menu();
				switch (option) {
				case 1:
					rent(staff);
					break;
				case 2:
					returnItem(staff);
					break;
				}
			}
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		} finally {
			try {
				DB.closeConnection();
			} catch (SQLException e) {
				System.err.println(e.getMessage());
			}
		}
	}
	
	public Staff login() throws SQLException {
		Staff staff = null;
		
		while (staff==null) {
			System.out.println("Introdueix nom d'usuari: ");
			String username = sc.nextLine();
			staff = Staff.byUsername(username);
		}
		System.out.println(staff);
		
		return staff;
	}
	
	public int menu() {
		int option=0;
		boolean ok=false;
		
		while (!ok) {
			System.out.println("Opcions\n\n1- Llogar\n2- Retornar\n3- Sortir");
			if (sc.hasNextInt()) {
				option = sc.nextInt();
				if (option<0 || option>3)
					System.out.println("Opció incorrecta");
				else
					ok=true;
			} else {
				System.out.println("Opció incorrecta");
			}
			sc.nextLine();
		}
		return option;
	}
	
	public void rent(Staff staff) throws SQLException {
		Customer customer = identifyCustomer();
		Inventory inventory = identifyInventory();
		DB.insertRental(customer, inventory, staff);
	}
	
	public void returnItem(Staff staff) throws SQLException {
		Inventory inventory = identifyInventory();
		Rental rental = Rental.byInventory(inventory);
		if (rental!=null) {
			rental.close();
			Customer customer = rental.customer();
			double balance = customer.getBalance();
			if (balance<=0)
				System.out.println("Aquest client no deu res.");
			else {
				System.out.println("Aquest client deu "+balance+". Vols cacel·lar el deute? (s/n)");
				String resp = sc.nextLine();
				if (resp.toUpperCase().equals("S")) {
					customer.cancelDebt(staff, balance);
				}
			}
		}
	}
	
	public Customer identifyCustomer() throws SQLException {
		Customer customer = null;
		while (customer == null) {
			System.out.println("Introdueix el cognom o el número del client: ");
			if (sc.hasNextInt()) {
				int id = sc.nextInt();
				sc.nextLine();
				customer = Customer.byId(id);
			} else {
				String lastName = sc.nextLine();
				List<Customer> customers = Customer.byLastName(lastName);
				if (customers.size()>1) {
					customer = chooseCustomer(customers);
				} else if (customers.size()==1)
					customer = customers.get(0);
			}
			if (customer==null) {
				System.out.println("No s'ha trobat cap client.");
			} else {
				System.out.println(customer);
			}
		}
		return customer;
	}
	
	public Customer chooseCustomer(List<Customer> customers) {
		int n=-1;
		int i=1;
		boolean ok=false;
		for (Customer c : customers) {
			System.out.println(i+"- "+c);
			i++;
		}
		while (!ok) {
			System.out.println("Tria un client");
			if (sc.hasNextInt()) {
				n=sc.nextInt();
				if (n<1 || n>customers.size())
					System.out.println("Opció incorrecta");
				else
					ok=true;
			} else {
				System.out.println("Opció incorrecta");
			}
			sc.nextLine();
		}
		return customers.get(n-1);
	}
	
	public Inventory identifyInventory() throws SQLException {
		Inventory inventory = null;
		boolean ok=false;
		int id;
		while (!ok) {
			System.out.println("Identifica l'ítem d'inventari: ");
			if (sc.hasNextInt()) {
				id = sc.nextInt();
				inventory = Inventory.byId(id);
				if (inventory!=null)
					ok=true;
				else
					System.out.println("Opció incorrecta");
			} else {
				System.out.println("Opció incorrecta");
			}
			sc.nextLine();
		}
		System.out.println(inventory);
		return inventory;
	}
}
