package mongodb.examen;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExExamen1 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("restaurants");
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Introdueix un codi postal: ");
		String zip = sc.nextLine();
		
		//db.restaurants.find({"address.zipcode":"11225", "cuisine":"Vegetarian"})
		List<Document> restaurants = coll.find(and(eq("address.zipcode", zip), eq("cuisine", "Vegetarian")))
				.into(new ArrayList<Document>());
		if (restaurants.isEmpty()) {
			System.out.println("No hi ha cap restaurant vegetarià en aquest codi postal.");
		} else {
			System.out.println("Restaurants vegetarians en aquest codi postal:");
			for (Document restaurant : restaurants) {
				Document address = restaurant.get("address", Document.class);
				System.out.println("Nom: "+restaurant.getString("name")+"  Adreça: "+
						address.getString("street")+" "+address.getString("building"));
			}
		}
		
		sc.close();
		client.close();
	}

}
