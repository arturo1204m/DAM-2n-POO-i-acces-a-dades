## Característiques

Utilitzem el terme **característica** (*feature*) com un terme general que
engloba tant les propietats com les operacions d'una classe.

### Propietats

Les **propietats** representen característiques estructurals d'una classe.
En general, les propietats tindran una equivalència més o menys directa
amb els atributs de la classe. Les propietats poden aparèixer en un
diagrama de dues formes completament diferents: com a *atributs* o com a
*associacions*.

Les propietats són quelcom que un objecte sempre pot proveir, i per
tant, no s'han d'utilitzar per modelar una relació temporal, com un
objecte que es passa com a paràmetre en la crida d'un mètode i que
s'utilitza només dins dels límits d'aquesta interacció.

#### Atributs

La notació en forma d'**atribut** descriu una propietat com una línia de
text dins de la caixa de la classe. La forma completa d'un atribut és:

`visibilitat nom: tipus [multiplicitat] = defecte {etiquetes}`

`- nom: String [1] = "Sense títol" {readOnly}`

#### Associacions

Una **associació** es representa com una línia contínua entre dues classes,
dirigida de la classe font a la classe destí. Es pot incloure el nom de la
propietat a l'extrem del destí, i també la seva multiplicitat.
L'extrem objectiu enllaça amb la classe que es correspon al
tipus de la propietat.

#### Atributs o associacions?

Atributs i associacions són dues formes diferents d'expressar les mateixes
propietats d'una classe. Per exemple, els dos diagrames següents mostren
gairebé la mateixa informació de les dues formes:

![Com atributs](docs/diagrames_uml/imatges/atribut_associacio.png)

![Com associacions](docs/diagrames_uml/imatges/atribut_associacio_001.png)

En el segon diagrama, menys compacte, podem veure alguna informació de més.
Per exemple, veiem que un nom només pot estar assignat a un personatge,
és a dir, que no hi poden haver dos personatges amb el mateix nom, o que
un ítem pot pertànyer o no a un personatge, però que una mascota ha de
pertànyer sempre a un personatge.

En general utilitzarem la notació en forma d'atribut per tipus primitius o
classes de les API de Java, i la notació com associació pels casos en què
també volem representar informació de la classe destí.

#### Interpretació de les propietats

Una propietat es pot interpretar habitualment com un atribut, però també com
al conjunt de l'atribut i els seus mètodes *get* i *set*. Així, per no
sobrecarregar un diagrama amb informació, és habitual no posar els mètodes
d'accés a un atribut i, si ambdós existeixen i són públics, posar directament
la visibilitat de la propietat com a pública, encara que realment sigui
privada a la implementació.

També és possible que existeixin mètodes *get* i *set* no associats a un
atribut. Per exemple, una figura geomètrica pot tenir un mètode *getArea()*
o una persona un mètode *getNomComplet()* i que realment no existeixi un
atribut *àrea*, sinó que es calculi en funció dels vèrtexs de la figura,
i que no existeixi un atribut *nomComplet*, sinó que s'obtingui a partir
de concatenar el nom i els cognoms.

En aquests casos, en el diagrama de classes seguiria essent convenient indicar
l'àrea i el nom complet com a propietats, perquè el fet que no ho siguin és
un detall d'implementació que és millor encapsular.

Si una propietat és multivaluada, implica que les dades s'estan
emmagatzemant en una col·lecció (habitualment una llista si l'ordre és
important, o un conjunt si no hi ha ordre lògic ni repeticions). En aquest
cas, la interfície que s'obté per a la propietat és diferent: no és només
un mètode *get* i un mètode *set*, sinó que hi poden haver una colla de mètodes
d'accés a la col·lecció.

Com que podem expressar les col·leccions fàcilment com a propietats
multivaluades, gairebé mai s'inclouen referències concretes a les
col·leccions en els diagrames.

Una associació bidireccional és un parell de propietats enllaçades com a
inverses. L'enllaç invers implica que si seguim ambdues propietats, hem
de retornar a un conjunt que contingui el punt inicial.

Quan es programen, cal assegurar que les dues propietats es mantenen
sincronitzades.

### Operacions

Les **operacions** són les accions que pot realitzar una classe i
habitualment es corresponen amb els mètodes de la classe. Habitualment
no es mostren les operacions que simplement manipulen propietats, ja que
normalment es poden inferir.

`visibilitat nom (paràmetres) : retorn {etiquetes}`

`+ balanceOn (date : Date) : Money`

Els paràmetres:

`direcció nom: tipus = valor per defecte`

Les operacions i atributs estàtics es representen subratllats.