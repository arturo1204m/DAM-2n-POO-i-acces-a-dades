# Ús bàsic del Git #

En aquesta guia es repassarà el flux de treball habitual
per treballar amb git i gitlab, amb la possibilitat de contribuir
i entregar els exercicis de manera ordenada.

- [Configuració inicial](configuracio_inicial.md)
- [Ordres bàsiques](ordres_basiques.md)
- [Tasques habituals](tasques_habituals.md)
